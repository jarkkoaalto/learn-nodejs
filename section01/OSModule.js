var os = require('os');

// display temp dir path
console.log("OS temp dir : " +os.tmpdir());

// display os name (home or workstation etc...)
console.log("Hostname : " + os.hostname());

// display os type 
console.log("OS Type" + os.type());

// display win64 or linux
console.log("OS platform" + os.platform());

// display :x64 or x32
console.log("OS Arch" + os.arch());

