var fs = require('fs');

try {
    fs.rmdirSync('devtmp');
    console.log("devtmp directory is removed");
}catch(err){
    if(err.code === "ENOENT"){
        console.log("Directory doesn't exist");
    }else if(err.code === "ENOTEMPTY"){
        console.log('Directory not empty');
    }else{
        console.log(err);
    }
}