var express = require('express');
var app = express();

app.get('/', function(req,res){
    res.send(' Hi there, This is Express on Node');
});

var server  = app.listen(8081, function(){
    var host = server.address().address;
    var port = server.address().port;
    console.log("Express App serving at %s : %s", host, port);
});