var assert = require('assert');

function add(a,b){
    return a+b;
}

var expected = add(1,3);

assert(expected == 4, "One + three = 4"); // true
// assert(expected == 5, "One + Three = 4"); // throw AssertinError
assert.notEqual(expected == 5,"One + Three = 4"); // true