var dbHost="127.0.0.1";
var dbPort=5984;
var dbName = 'testdb';
var couchdb = require('felix-couchdb');
var client = couchdb.createClient(dbPort, dbHost);
var db = client.db(dbName);

db.getDoc('2609',function(err,doc){
    doc.name = "Jack";
    doc.email = "jack@inbox.com";
    db.saveDoc('2609',doc);


    db.getDoc('2609', function(err,revisedUser){
        console.log(revisedUser);
    });
});