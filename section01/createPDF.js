var pdf = require('pdfkit');
var fs = require('fs');

var MyDoc = new pdf;

MyDoc.pipe(fs.createWriteStream('node.pdf'));

MyDoc.font('Times-Roman')
    .fontSize(48)
    .text('Nodejs PDF Document',100,100);

MyDoc.end();