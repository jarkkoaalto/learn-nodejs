var fs = require('fs');
var data = 'NodeJS stream tutorial';

var writeStream = fs.createWriteStream('output.txt');
writeStream.write(data,'UTF8');

writeStream.end()

writeStream.on('finish',function(){
    console.log('File Write completed');
});

writeStream.on('error',function(){
    console.log(err.stack);
});