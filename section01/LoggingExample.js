
// example different log messages

console.time('timer');
console.log('This is NodeJs logging example');
console.info("This is info message");
console.warn("This is warning");
console.error("Thisis error message");
console.dir(console);
console.timeEnd('timer');

