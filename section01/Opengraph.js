var ograph = require('open-graph')

ograph('http://ogp.me/', function(err,result){
    console.log(result);
});

/**
 *  RESULT
 * ==================================================
 * 
 * 
 * { title: 'Open Graph protocol',
  type: 'website',
  url: 'http://ogp.me/',
  image:
   { url: 'http://ogp.me/logo.png',
     type: 'image/png',
     width: '300',
     height: '300',
     alt: 'The Open Graph logo' },
  description:
   'The Open Graph protocol enables any web page to become a rich object in a social graph.' }
 */