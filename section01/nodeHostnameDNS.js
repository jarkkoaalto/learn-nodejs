var dns = require('dns');

dns.lookup('www.google.com', function onLookup(err, address, faminly){
    console.log('Address : ', address);
    dns.reverse(address, function(err, hostnames){
        if(err){
            console.log(err.stack);
        }
        console.log('Reverse for Address : ' + address + " : " + JSON.stringify(hostnames));
    });
});

/**
 * RESULT:
 * -----------------------------------------------------
 * Address :  64.233.165.147
Reverse for Address : 64.233.165.147 : ["lg-in-f147.1e100.net"]
 */